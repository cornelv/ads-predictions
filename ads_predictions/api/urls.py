from django.urls import path

from .views import GetPredictionsView

urlpatterns = [
    path('get-predictions/<int:account_id>/<str:date>/', GetPredictionsView.as_view(), name='get_predictions'),
]