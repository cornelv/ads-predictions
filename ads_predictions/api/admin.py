from django.contrib import admin
from .models import Account, Campaign, Result, WeatherCondition

admin.site.register(Account)
admin.site.register(Campaign)
admin.site.register(Result)
admin.site.register(WeatherCondition)