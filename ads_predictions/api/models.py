from django.db import models

# Create your models here.

class Account(models.Model):

    def __str__(self):
        return str(self.id)

class Campaign(models.Model):

    account = models.ForeignKey(Account, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.id)


class Result(models.Model):

    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)

    date = models.DateField()

    adgroup = models.IntegerField()
    keyword = models.IntegerField()

    clicks = models.IntegerField()
    impressions = models.IntegerField()

    def __str__(self):
        return "{} - {}".format(self.campaign.id, self.date)


class WeatherCondition(models.Model):

    date = models.DateField()

    precip_probability = models.FloatField()
    wind_speed = models.FloatField()
    sky_coverage = models.FloatField()
    temperature = models.FloatField()

    def __str__(self):
        return str(self.date)
