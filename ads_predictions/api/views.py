from django.http import JsonResponse
from django.views import View
from django.shortcuts import get_object_or_404
import datetime
from django.utils.dateparse import parse_date
from django.db.models import Sum, F
from django.db.models.expressions import Window
from sklearn import linear_model
from api.darksky import Darksky
from api.models import WeatherCondition, Account, Result

class GetPredictionsView(View):
    def get(self, request, account_id, date):

        pdate = parse_date(date)
        today = datetime.date.today()
        max_date = today + datetime.timedelta(days=7)

        # validate the data
        if pdate < today or pdate > max_date:
            return JsonResponse({
                "success": False,
                "error": "Invalid date provided, the date must be today or in the next 7 days"
                })

        # get weather predictions
        try: 
            weather_condition = WeatherCondition.objects.get(date=pdate)
        except WeatherCondition.DoesNotExist:
            darksky = Darksky()
            data = darksky.get_forecast(pdate)
            weather_condition = WeatherCondition()

            weather_condition.date = pdate
            weather_condition.precip_probability = data['precipProbability']
            weather_condition.wind_speed = data['windSpeed']
            weather_condition.sky_coverage = data['cloudCover']
            weather_condition.temperature = data['temperatureHigh']

            weather_condition.save()

        # we will use this later for predictions
        req_weather_data = [
            #weather_condition.precip_probability, 
            weather_condition.wind_speed,
            weather_condition.sky_coverage,
            weather_condition.temperature
        ]

        account = get_object_or_404(Account, pk=account_id)

        # prepare the data
        # get the structured weather conditions for all days
        days = Result.objects.filter(campaign__account=account).distinct().values_list('date')
        weather_conditions = {}
        for day in WeatherCondition.objects.filter(date__in=days):
            weather_conditions[ day.date.strftime("%Y%m%d") ] = [
                #day.precip_probability,
                day.wind_speed,
                day.sky_coverage, 
                day.temperature
            ]
        
        # agregate the daily sum for every campaign
        results = Result.objects.filter(campaign__account=account).values(
            "date", "campaign").annotate(date_clicks=Sum("clicks"), date_impressions=Sum('impressions'))

        predictions = []
        for campaign in account.campaign_set.all():

            independent_values = []
            dependent_clicks = []
            dependent_impressions = []
            campaign_predictions = {"campaign_id": campaign.id}

            for result in results:
                if result['campaign'] != campaign.id:
                    continue
                
                dependent_clicks.append(result['date_clicks'])
                dependent_impressions.append(result['date_impressions'])
                independent_values.append( weather_conditions[result['date'].strftime("%Y%m%d")] )


            # calculate predicted clicks using linear regression
            regr = linear_model.LinearRegression()
            regr.fit(independent_values, dependent_clicks) # train the model

            # Make predictions using the weather data
            campaign_predictions['predicted_clicks'] = int(regr.predict([req_weather_data])[0])

            # calculate predicted impressions
            regr = linear_model.LinearRegression()
            regr.fit(independent_values, dependent_impressions)
            campaign_predictions['predicted_impressions'] = int(regr.predict([req_weather_data])[0])

            predictions.append(campaign_predictions)
        
        return JsonResponse({"success": True, 'predictions': predictions})