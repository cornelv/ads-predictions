import requests
from django.conf import settings
import time

class Darksky(object):

    base_url = "https://api.darksky.net/forecast/"

    def __init__(self):
        self.api_key = settings.DARKSKY_API_KEY
        self.location = settings.DARKSKY_LOCATION
    
    def get_forecast(self, date):
        # send the date as a date object

        timestamp = int(time.mktime(date.timetuple()))

        url = "{base_url}{api_key}/{location},{date}".format(
            base_url=self.base_url, 
            api_key=self.api_key, 
            location=self.location, 
            date=timestamp)

        r = requests.get(url)
        data = r.json()

        # assume the data is correct, this will throw exception otherwise
        return data['daily']['data'][0]
