FROM python:3.6-alpine

ENV PYTHONUNBUFFERED 1

RUN apk update \
  && apk add gcc python3-dev musl-dev g++ lapack-dev gfortran lapack libstdc++ py3-scipy

COPY . /app
WORKDIR /app

RUN pip install --no-cache-dir -r /app/requirements.txt

EXPOSE 8000

ENTRYPOINT ["/app/entrypoint.sh"]

CMD python ads_predictions/manage.py runserver 0:8000