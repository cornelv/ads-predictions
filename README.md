# ads-predictions

## Run the app

Requirements: docker

1. Clone the image
1. cd into project root folder
3. Build the image `docker build -t ads-predictions .`
4. Run the command `docker run -it --rm -v $(pwd):/app -p 8000:8000 ads-predictions`
5. Open browser and access `http://localhost:8000/api/get-predictions/<account_id>/<date:YYYY-MM-DD>/`

To close the application press `Ctr + C` in terminal or stop the docker container.

Auth is not implemented, but django admin is available without authentication at `http://localhost:8000/admin/`

## Development

For development run the container with this command:
`docker run -it --rm -v $(pwd):/app -p 8000:8000 ads-predictions sh`
